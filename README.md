# README #

This repo contains working files for seanbuchholz.com, my personal portfolio website. It will be updated as I a) add items to my portfolio or b) learn and develop new web design techniques that I wish to implement or utilize on this website

### What is this repository for? ###

* In-process HTML pages
* Non-production Sass files
* Un-minified CSS and JavaScript
* Other development and non-production artifacts

### Who do I talk to? ###

* Email: sean.buchholz@me.com -OR- smbuchholz@fullsail.org
* Twitter: @seanmbuchholz